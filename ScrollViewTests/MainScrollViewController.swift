//
//  ViewController.swift
//  ScrollViewTests
//
//  Created by Victor Magalhaes on 17/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class MainScrollViewController: UIViewController {
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    var viewControllers = [BaseAnimatedViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        insertViews()
        setupLayout()
    }
    
    private func insertViews(){
        self.view.addSubview(scrollView)
        viewControllers.append(ViewController1())
        viewControllers.append(ViewController2())
        viewControllers.append(ViewController3())
    }
    
    private func setupLayout() {
        scrollView.frame.size = view.frame.size
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        for viewController in viewControllers {
            addViewController(viewController)
        }
        
        setupConstraints()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.contentSize = CGSize(
            width: view.frame.width,
            height: view.frame.height * CGFloat(Float(viewControllers.count) - 0.2)
        )
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            scrollView.heightAnchor.constraint(equalToConstant: view.frame.height),
            scrollView.widthAnchor.constraint(equalToConstant: view.frame.width),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        
        for (pos, viewController) in viewControllers.enumerated() {
            addViewController(viewController)
            let multiplier = pos == 0 ? CGFloat(0.8) : CGFloat(1.0)
            let anchor = pos == 0 ? scrollView.topAnchor : viewControllers[pos-1].view.bottomAnchor
            
            NSLayoutConstraint.activate([
                viewController.view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: multiplier),
                viewController.view.widthAnchor.constraint(equalTo: view.widthAnchor),
                viewController.view.topAnchor.constraint(greaterThanOrEqualTo: anchor)
                ])
        }
    }
    
    private func addViewController(_ viewController: UIViewController) {
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(viewController.view)
        addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
    }
    
    private func currentPos() -> Int {
        return Int(scrollView.contentOffset.y/view.frame.height)
    }
}

extension MainScrollViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 { scrollView.setContentOffset(CGPoint.zero, animated: false) }
        let factor = ((scrollView.contentOffset.y - (CGFloat(currentPos()) * view.frame.height))/view.frame.height)
        viewControllers[currentPos()+1].animate(factor: factor)
    }
}

